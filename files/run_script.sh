# script.py 
# arg1: inputfolder
# arg2: outputfolder
# arg3: temp_folder

# write to local results folder first and copy folder after run?

sudo ufw default deny outgoing
sudo ufw default deny incoming 
sudo ufw allow ssh
sudo ufw allow from 10.10.10.0/24 to any port 445
sudo ufw allow out to 10.10.10.0/24 port 445

echo "y" | sudo ufw enable

cd /script

# sudo -u toolsuser strace -o /script/strace_output.txt  -e trace=network,openat  /script/tools_to_data/bin/python3 /script/script.py -i $1 -o $2 -t $3 1> stdout.txt 2> stderr.txt

sudo -u toolsuser /script/tools_to_data/bin/python3 /script/script.py -i $1 -o $2 -t $3 1> stdout.txt 2> stderr.txt


# cp /script/strace_output.txt $2/log
cp stdout.txt $2/log
cp stderr.txt $2/log


# sudo ufw allow out for access to user-server for send mail
sudo ufw allow out to 108.128.176.164 port 443
sudo ufw allow out to 54.195.171.33 port 443
sudo ufw allow out to 99.81.12.38 port 443
sudo ufw allow out to 145.100.29.146 port 53


# send mail using API

CO_TOKEN="$(cat /etc/rsc/workspace.json | jq --raw-output .co_token)"
OWNER_ID="$(cat /etc/rsc/workspace.json | jq --raw-output .owner_id)"
MESSAGE_API="$(cat /etc/rsc/workspace.json | jq --raw-output .co_user_api_endpoint)${OWNER_ID}/send_message/"
export WORKSPACE_NAME="$(cat /etc/rsc/workspace.json | jq --raw-output .workspace_name)"
export WORKSPACE_ID="$(cat /etc/rsc/workspace.json | jq --raw-output .workspace_id)"
MESSAGE="message=$(envsubst </script/email.txt)"
curl --request POST --header "Authorization: ${CO_TOKEN}" --data "category=workspace_monitoring" --data "subject=Tools-to-data workspace has finished" --data "${MESSAGE}" "${MESSAGE_API}"


TIMESTAMP=$(date +%Y-%m-%d-%H%M)

echo  "$TIMESTAMP Finished" >> $2/log/report.txt

disown